@extends('adminlte.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cast</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/cast">Home</a></li>
              <li class="breadcrumb-item active">Cast Index</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">List of Cast</h3>
                </div>
              <!-- /.card-header -->



              <div class="card-body">
                @if(session('success'))
                  <div class="alert alert-success">{{ session('success') }}</div>
                @endif
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @forelse($casts as $db => $cast)
                    <tr>
                      <td>{{ $db + 1 }}</td>
                      <td>{{ $cast->nama }}</td>
                      <td>{{ $cast->umur }}</td>
                      <td>{{ $cast->bio }}</td>
                      <td style="display:flex;">
                        <a href="{{ route('cast.show', ['cast' => $cast->id]) }}" class="btn btn-info btn-sm" style="margin-right:8px;">Show</a> <!-- /cast/{{ $cast->id }} -->
                        <a href="/cast/{{ $cast->id }}/edit" class="btn btn-primary btn-sm" style="margin-right:8px;">Edit</a>
                        <form action="/cast/{{ $cast->id }}" method="POST" style="margin-right:8px;">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="4" align="center">No one here yet.</td>
                    </tr>
                    @endforelse

                  </tbody>
                </table>
              </div>

              
                 <!-- /.card-body -->
                <div class="card-footer">
                  <a href="/cast/create" class="btn btn-primary">Create</a>
                </div>
            </div>
            <!-- /.card -->
            
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
